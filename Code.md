This can be used for either type of plot--it's agnostic about the time variable going,
and the lag can be set.

```r
mapPlot = function(myframe = useful, mytime = 654100, timelag = 34, timevar = "time", ...) {
  # I define half the plot here, and half later. No real reason.
  # This is why I don't usually share that much code.
  myframe$timevar = get(timevar, myframe)
  myframe = myframe[myframe$timevar >= (mytime - timelag) & myframe$timevar <= mytime, ]
  if (timevar == "time") {
    myframe$group = myframe$voyageID
  }
  if (timevar == "yearday") {
    myframe$group = paste(myframe$voyageID, myframe$Year)
  }
  
  ggplot(myframe, aes(y = Lat, x = Long)) + 
    geom_polygon(data = world, aes(x = x, y = y, group = id), drop = T) + 
    geom_path(size = 0.5, aes(group = group,
      #This alpha here decaying by time is what makes the trails slowly erase.
      alpha = (timevar - mytime) / timelag, color = color)) + 
      scale_color_identity() +
      geom_point(data = myframe[myframe$timevar == mytime, ], size = 0.75, aes(group = voyageID,
        alpha = (timevar - mytime) / timelag, color = color)) +
        ylab("")+
        xlab("") +
        opts(legend.position = "none",
          axis.text.x = theme_blank(),
          axis.text.y = theme_blank(),
          axis.ticks = theme_blank()) +
        coord_map(...)
}